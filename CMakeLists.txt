cmake_minimum_required(VERSION 3.5)

set(LIB_NAME "parameter")

project(${LIB_NAME} LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(LIB_SRC
  src/value_base.cpp
  inc/value_base.h
  src/value.cpp
  inc/value.h
  src/value_complex.cpp
  inc/value_complex.h
  src/value_selection.cpp
  inc/value_selection.h
  src/simple_base64.cpp
  inc/simple_base64.h
  src/value_hash.cpp
  inc/value_hash.h
  inc/parameter_value.h
  inc/callback_monitor.h
) 

add_library(${LIB_NAME} SHARED ${LIB_SRC})

list(APPEND Libs pugixml )

target_link_libraries(${LIB_NAME} PRIVATE ${Libs})

target_include_directories(${LIB_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/inc)

include(GenerateExportHeader)

generate_export_header(
  ${LIB_NAME}
  EXPORT_MACRO_NAME   "PARAMETER_EXPORT"
  EXPORT_FILE_NAME    "${CMAKE_CURRENT_SOURCE_DIR}/inc/parameter_export.h")
