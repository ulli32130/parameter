#pragma once

#include <string>
#include <atomic>
#include <type_traits>
#include <variant>
#include <memory>

#include "value_base.h"


class ValueComplex;

template<typename T>
class Value : public ValueBase {

	public:
    typedef T value_type;
	typedef std::shared_ptr<Value<T>>	PTR_T;		///< shared pointer to Value
	typedef std::function<void(PTR_T)>	CBREF_T;	///< callback function type
	typedef const CBREF_T*				CB_PTR;		///< reference to callback function type

    private:
  
    PTR_T	m_minValue;								///< shared pointer to minimum value
    PTR_T	m_maxValue;								///< shared pointer to maximum value

    public:

	/**
	 * Constructor
	 * \param name
	 * name of value
	 * \param initialValue
	 * initial value
	 * \param min
	 * minimum value, default NULL if not needed.
	 * \param max
	 * maximum value, default NULL if not needed
	 */
	Value(const std::string& name, T initialValue, PTR_T min = nullptr, PTR_T max = nullptr)
		: ValueBase(name)
		, m_minValue(min)
		, m_maxValue(max)
	{
		m_container.emplace<T>();
		m_container = initialValue;

		m_valType = whichType<T>();

		if (min != NULL) {
			addChild(min);
			setMinContainer(&min->setContainer());
		}
		if (max != NULL) {
			addChild(max);
			setMaxContainer(&max->setContainer());
		}

	}

	/**
	 * Checks if value has a range.
	 * \return 
	 * true, if value has a range, otherwise false
	 */
    bool hasRange() override
    { if ((m_minValue != NULL) && (m_maxValue != NULL)) { return true; } else { return false; } }

	/**
	 * Add a callback reference. This reference is called whenever the value changes
	 * \param client
	 * reference to callback function.
	 */
    void addValueChangedCallback(CB_PTR client) {
		ValueBase::addValueChangedCallback(reinterpret_cast<ValueBase::CB_PTR>(client));
    }

	/**
	 * Remove a callback reference.
	 * \param client
	 * reference to callback function.
	 */
    void removeValueChangedCallback(CB_PTR client) {
        ValueBase::removeValueChangedCallback(reinterpret_cast<ValueBase::CB_PTR>(client));
    }

	/**
	 * Get range, minimum value.
	 * \return 
	 * minumum value.
	 */
    T getMin() { if (m_minValue != 0) { return m_minValue->get(); } else { return static_cast<T>(0); } }

	/**
	 * Get range, maximum value.
	 * \return 
	 * maximum value.
	 */
    T getMax() { if (m_maxValue != 0) { return m_maxValue->get(); } else { return static_cast<T>(0); } }
  
	/**
	 * Get value.
	 * \return 
	 * value.
	 */
    T get() {
        const std::lock_guard<std::recursive_mutex> lock(m_accessLock);
        return std::get<T>(m_container); }

	/**
	 * Set value. 
	 * \param val
	 * New value.
	 * \return 
	 * False, if range exceeds, otherwise true.
	 */
    bool set(T val) {
        const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

        if (m_minValue != nullptr && m_maxValue != nullptr) {
            if (val < m_minValue->get()) {
                return false;
            }
            if (val > m_maxValue->get()) {
                return false;
            }
        }

        if (std::get<T>(m_container) == val) {
            return true;
        }
        m_container = val;
        onValueChanged();
        return true;
    }

	/**
	 * Write value to an xml file.
	 * 
	 * \param node
	 * reference to node.
	 */
    void PARAMETER_EXPORT writeValue(pugi::xml_node& node);

	/**
	 * Read value from an xml file.
	 * 
	 * \param attr
	 * reference to attribute.
	 * \param valueChanged 
	 * is set to true if value has changed
	 */
    void PARAMETER_EXPORT readValue(pugi::xml_attribute& attr, bool& valueChanged);

	/**
	 * Write node and all childs to an xml file.
	 * \param 
	 * reference to node
	 * \return 
	 * true: success, false: error
	 */
    bool write(pugi::xml_node& parent) override
    {
        if (isVolatile())
            return true;

        pugi::xml_node nodeChild = parent.append_child(m_typeNameMap[m_valType].c_str());

        nodeChild.append_attribute(idName) = m_valName.c_str();
        setValueType(nodeChild);
        setValueSize(nodeChild, sizeof(T));
        Value<T>::writeValue(nodeChild);

        for (auto it : m_child) {
            it->write(nodeChild);
        }

        return true;
    }

	/**
	 * Read node and all childs from an xml file.
	 * 
	 * \param node
	 * reference to node
	 * \return 
	 * true: success, false: error.
	 */
    bool read(pugi::xml_node& node) override
    {
        if (isVolatile())
            return true;

        bool valChanged = false;

        for (pugi::xml_node child : node.children()) {
            pugi::xml_attribute attr = child.attribute(idName);

            if (attr != NULL) {
                const char* name = attr.as_string();
				if (0 == strcmp(name, ValueBase::idMinValue)) {
					if (m_minValue == nullptr) {
						m_minValue = Value<T>::create(idMinValue, get());
						addChild(m_minValue);
					}
					m_minValue->read(child);
				}
                else if (0 == strcmp(name, ValueBase::idMaxValue)) {
					if (m_maxValue == nullptr) {
						m_maxValue = Value<T>::create(idMaxValue, get());
						addChild(m_maxValue);
					}
                    m_maxValue->read(child);
                }
            }
        }

        pugi::xml_attribute nodeAttr = node.attribute(idName);
        if (nodeAttr == NULL) {
            //WARNING("name attribute missing ...\n");
            return false;
        }

        if (0 == getValueName().compare(nodeAttr.as_string())) {
            // thats my node, read attributes ...
            pugi::xml_attribute attr = node.attribute(idValue);

            if (attr == NULL) {
                //WARNING("value attribute missing ...\n");
                return false;
            }

            Value<T>::readValue(attr, valChanged);

            if (valChanged) {
                onValueChanged();
            }
            return true;
        }
        return false;
    }

	/**
	 * Copy object.
	 * 
	 * \param src
	 * shared pointer to source object
	 * \return 
	 * true: copy succeeded, false: copy failed
	 */
    bool copy(std::shared_ptr<ValueBase> src) override
    {
        const std::lock_guard<std::recursive_mutex> lock(m_accessLock);
        if (this->getValueType() == src->getValueType()) {

			auto dstObj = dynamic_cast<Value<T>*>(src.get());
			auto srcObj = dynamic_cast<Value<T>*>(this);

			dstObj->set(srcObj->get());

            return true;
        }
        return false;
    }

	/**
	 * Compare if src equals object.
	 * \param src
	 * Shared pointer to source object
	 * \return 
	 * true: reference to src equals object 
	 */
    bool compare(std::shared_ptr<ValueBase> src) override
    {
        if (src == nullptr) {
            return false;
        }

        const std::lock_guard<std::recursive_mutex> lock(m_accessLock);
        if (this->getValueType() == src->getValueType()) {

			auto me		= dynamic_cast<Value<T>*>(this);
			auto you	= dynamic_cast<Value<T>*>(src.get());

			if (me->get() == you->get()) {
				return true;
			}
        }
        return false;
    }

	/**
	 * Add hash value from object to hash.
	 * \param hash
	 * reference to hash value
	 */
    void addHash(ValueHash& hash) override;

	/**
	 * Get shared pointer to child.
	 * \param name
	 * The name/path to the child
	 * \param ok
	 * True, if child is accessable, otherwise false
	 * \return 
	 * shared pointer to child
	 */
	PTR_T getChild(const std::string& name, bool& ok )
	{

		return nullptr;
	}

	/**
	 * Cast from ValueBase
	 * \param base
	 * pointer to ValueBase to cast from
	 * \return 
	 * shared pointer to type cast
	 */

	static inline PTR_T cast(const std::shared_ptr<ValueBase>& base) {
		return std::dynamic_pointer_cast<Value<T>>(base);
	}

	/**
	 * Create a new item if the parent has no child with name <name> or take the existing item
	 * from the parent. 
	 * \param parent
	 * reference to parent, my be null if item is orphaned
	 * \param name
	 * the name of the item
	 * \param initialValue
	 * initial value of item
	 * \param min
	 * pointer to minimum value item
	 * \param max
	 * pointer to maximum value item
	 * \return 
	 * shared pointer to item
	 */
	static PTR_T create(const ValueBase::PTR_T& parent, const std::string name, const T initialValue,
		PTR_T min = nullptr, PTR_T max = nullptr ) {

		// no parent, generate a new item
		if(!parent.get()) {
			return create(name, initialValue);
		}

		// ask parent if he knows an item with this name
		const auto& child = parent->getChild(name);

		if(child) {
			if (child->getType() != ValueBase::whichType<T>()) {
				// the types don't match ....
				// TODO: error handling
				printf("type missmatch error !!\n");
				return nullptr;
			}

			// return this item
			return Value<T>::cast(child);
		}

		// generate new item
		PTR_T p = std::make_shared<Value<T>>(name, initialValue, min, max);
		parent->addChild(p);
		p->setParent(parent);
		return p;
	}

	/**
	 * create
	 * create a new item without parent 
	 * \param name
	 * the name of the item
	 * \param initialValue
 	 * initial value of item
	 * \param min
	 * pointer to minimum value item
	 * \param max
	 * pointer to maximum value item
	 * \return 
	 * shared pointer to item
	 */
	static PTR_T create(const std::string name, const T initialValue, 
		PTR_T min = nullptr, PTR_T max = nullptr ) {
		return std::make_shared<Value<T>>(name, initialValue, min, max);
	}

	/**
	 * createMin.
	 * create item for range check, minimum value item
	 * \param minValue
	 * the minimum value 
	 * \return 
	 * shared pointer to item
	 */
	static PTR_T createMin(const T minValue) {
		return std::make_shared<Value<T>>(ValueBase::idMinValue, minValue);
	}

	/**
	 * createMax.
	 * create item for range check, maximum value item
	 * \param maxValue
	 * the maximum value
	 * \return
	 * shared pointer to item
	 */
	static PTR_T createMax(const T maxValue) {
		return std::make_shared<Value<T>>(ValueBase::idMaxValue, maxValue);
	}

	/**
	 * Assigns the value of base to v, if base and v are the same type.
	 * \param base
	 * source
	 * \param v
	 * destination
	 * \return 
	 * true, if the value of base can be assigned to v, otherwise false is returned 
	 */
	//template<typename T>
	static inline bool get(const std::shared_ptr<ValueBase>& base, T& v) {

		if (base->getType() == whichType<T>()) {
			v = Value<T>::cast(base)->get();
			return true;
		}

		return false;
	}

};
