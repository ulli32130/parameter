#pragma once

#include "value_base.h"
#include "value.h"

class CallbackMonitorItemBase
{
public:
	virtual ~CallbackMonitorItemBase()  { ; }
};

template<typename T>
class CallbackMonitorItem : public CallbackMonitorItemBase
{
private:
	std::shared_ptr<Value<T>>							m_sender;
	std::function<void(std::shared_ptr<Value<T>>)>		m_cbRef;

public:
	CallbackMonitorItem(std::shared_ptr<Value<T>> sender, std::function<void(std::shared_ptr<Value<T>>)> cb)
		: m_sender(sender)
		, m_cbRef(cb)
	{
		m_sender->addValueChangedCallback(&m_cbRef);
	}

	virtual ~CallbackMonitorItem()
	{
		m_sender->removeValueChangedCallback(&m_cbRef);
	}
};

class CallbackMonitor
{
private:
	std::vector<CallbackMonitorItemBase*>	m_itemList;

public:

	template<typename T>
	void connect(std::shared_ptr<Value<T>> sender, std::function<void(std::shared_ptr<Value<T>>)> cb)
	{
		m_itemList.push_back(new CallbackMonitorItem(sender, cb));
	}

	~CallbackMonitor()
	{
		for (const auto& item : m_itemList) 
		{
			delete item;
		}
	}
};

