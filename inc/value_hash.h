#pragma once

#include <array>

#ifdef DYNAMIC_LIB
#include "parameter_export.h"
#else    
#define PARAMETER_EXPORT
#endif    

class PARAMETER_EXPORT ValueHash {

private:
    enum { HashBlockSize = 512 / 8, HashBytes = 32 };

    /// size of processed data in bytes
    uint64_t                             m_hashNumBytes;
    /// valid bytes in m_buffer
    size_t                               m_hashBufferSize;
    /// bytes not processed yet
    uint8_t                              m_hashBuffer[HashBlockSize];

    enum { HashValues = HashBytes / 4 };
    /// hash, stored as integers
    uint32_t m_hash[HashValues];


public:
	ValueHash();

	void add(const void* data, size_t numBytes);
    void getHash(uint8_t buffer[ValueHash::HashBytes]);
    std::string getHash();
    void hashReset();

private:

    void processHashBlock(const void* data);
    void processHashBuffer();
};