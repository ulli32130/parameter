#pragma once


#include <vector>
#include <string>


class simpleBase64 {

private:
	static const std::string base64_chars;

public:
	static std::vector<unsigned char>	base64_decode(std::string const& encoded_string);
	static std::string					base64_encode(const unsigned char* buf, unsigned int bufLen);

	static inline bool is_base64(unsigned char c) {
		return static_cast<bool>( (isalnum(c) || (c == '+') || (c == '/')) );
	}
};