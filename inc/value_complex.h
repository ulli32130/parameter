#pragma once

#include "value_base.h"
#include <list>
#include <vector>
#include <memory>


/**
 * ValueComplex.
 */
class PARAMETER_EXPORT ValueComplex : public ValueBase {

protected:

#pragma warning( push )
#pragma warning( disable: 4251 )
   
#pragma warning( pop )

public:
	typedef std::shared_ptr<ValueComplex>		PTR_T;		///< shared pointer to ValueComplex

	/**
	 * Constructor.
	 * 
	 * \param name
	 * name of node.
	 */
	ValueComplex(std::string name);
    virtual ~ValueComplex();

	/**
	 * Create a new item if the parent has no child with name <name> or take the existing item
	 * from the parent.
	 * \param parent
	 * parent node
	 * \param name
	 * name of node
	 * \return 
	 * shared pointer to node
	 */
	static PTR_T create(ValueBase::PTR_T parent, std::string name);

	/**
	 * Creates a new node.
	 * 
	 * \param name
	 * name of node
	 * \return 
	 * shared pointer to node
	 */
	static PTR_T create(std::string name);


    static ValueBase::PTR_T readChild(ValueBase::PTR_T parent, pugi::xml_node& node);

    bool write(pugi::xml_node& node)                            override;
    bool read(pugi::xml_node& node)                             override;
    void addHash(ValueHash& hash)                               override;

	/**
	 * Cast ValueBase::PTR_T to ValueComplex::PTR_T .
	 * 
	 * \param base
	 * ValueBase::PTR_T
	 * \return 
	 * ValueComplex::PTR_T
	 */
	static inline PTR_T cast(std::shared_ptr<ValueBase>& base) {
		return std::dynamic_pointer_cast<ValueComplex>(base);
	}
};
