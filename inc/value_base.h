#pragma once

#include <pugixml.hpp>

#include <cstdint>
#include <sstream>
#include <cstring>
#include <list>
#include <functional>
#include <mutex>
#include <map>
#include <variant>
#include <memory>

#include "value_hash.h"

#ifdef DYNAMIC_LIB
    #include "parameter_export.h"
#else    
    #define PARAMETER_EXPORT
#endif    

class ValueComplex;

enum class VALUE_TYPE : uint32_t {
    INVALID,		///< identifies an invalid type
    USER,			///< identifies a user type, not used yet
    COMPLEX,		///< identifies tree node, see ValueComplex
    INT8,			///< identifies int 8 bit value
    UINT8,			///< identifies unsigned int 8 bit value
    INT16,			///< identifies int 16 bit value
    UINT16,			///< identifies unsigned int 16 bit value
    INT32,			///< identifies int 32 bit value
    UINT32,			///< identifies unsigned int 32 bit value
    INT64,			///< identifies int 64 bit value
    UINT64,			///< identifies unsigned int 64 bit value
    FLOAT,			///< identifies float value
    DOUBLE,			///< identifies double value
    BOOL,			///< identifies bool value
    STRING,			///< identifies std::string
    SELECTION,		///< identifies selection value see ValueSelection
    SELECTED_ITEM,  ///< identifies .... ( grmpf, forgot this )
    ITEM_LIST		///< identifies item list
};

/**
 * The PropertyContainer std::variant holds the data.
 */
using PropertyContainer
= std::variant<int8_t, uint8_t, int16_t, uint16_t, int32_t, uint32_t, int64_t, uint64_t, bool, float, double, std::string>;

#pragma warning( push )
#pragma warning( disable: 4251 )
class PARAMETER_EXPORT ValueBase : public std::enable_shared_from_this<ValueBase> {
#pragma warning ( pop )


public:
	typedef std::shared_ptr<ValueBase>			PTR_T;		///< shared pointer to ValueBase
	typedef const std::function<void(PTR_T)>*	CB_PTR;		///< reference to callback function


protected:

#pragma warning( push )
#pragma warning( disable: 4251 )
	PropertyContainer	m_container;		///< container, std::variant of value
	PropertyContainer*  m_minContainer;		///< reference to minimum std::variant
	PropertyContainer*  m_maxContainer;		///< reference to maximum std::variant
#pragma warning ( pop )

	//###############################################

    bool			m_volatile;					///< this object is not stored to an xml file if m_volatile is true
	bool			m_checkDuplicateEntries;	///< a child can be added repeatedly if m_checkDuplicateEntries is false
    VALUE_TYPE		m_valType;					///< identifies the type 
    uint32_t		m_flags;					///< flags

    #pragma warning( push )
    #pragma warning( disable: 4251 )

	PTR_T													m_parent;				///< shared pointer to parent
    std::string												m_valName;				///< name
    std::vector<PTR_T>										m_child;				///< vector of childs
    std::recursive_mutex									m_accessLock;			///< mutex for save access
    std::list<CB_PTR>										m_plistener;			///< list to callback references
    std::list<const std::function<void(PTR_T, PTR_T)>*>		m_childAddedCallbacks;	///< list of child added callback references
    std::map<std::string, PropertyContainer>				m_propertys;			///< map of propertiers			
    static std::map<VALUE_TYPE, std::string>				m_typeNameMap;			
    #pragma warning ( pop )

public:

	/**
	 * Access to container, get container.
	 * \return
	 * value container 
	 */
	const PropertyContainer& getContainer() { return m_container;  }

	/**
	 * Access to container, set container.
	 */
	PropertyContainer& setContainer() { return m_container; }

	/**
	 * Set range, minimum container.
	 * \param ref
	 * reference to minimum container
	 */
	void setMinContainer(PropertyContainer* ref) {
		m_minContainer = ref;
	}

	/**
	 * Set range, maximum container.
	 * \param ref
	 * reference to maximum container
	 */
	void setMaxContainer(PropertyContainer* ref) {
		m_maxContainer = ref;
	}

    // XML identifier
    static constexpr const char* idName            = "Name";			///< identifier for xml attribute name
    static constexpr const char* idType            = "Type";			///< identifier for xml attribute type
    static constexpr const char* idDataSize        = "DataSize";		///< identifier for xml attribute data size
    static constexpr const char* idValue           = "Value";			///< identifier for xml attribute value
    static constexpr const char* idHasRange        = "HasRange";		///< identifier for xml attribute has range
    static constexpr const char* idMinValue        = "MinValue";		///< identifier for xml attribute minimum value
    static constexpr const char* idMaxValue        = "MaxValue";		///< identifier for xml attribute maximum value
    static constexpr const char* idItems           = "Items";			///< identifier for xml attribute items
    static constexpr const char* idBase64Value     = "base64Value";	///< identifier for xml attribute base 64 encoding
    static constexpr const char* idProperties      = "PROPERTIES";	///< identifier for xml attribute properties
    static constexpr const char* idProperty        = "PROPERTY";		///< identifier for xml attribute property

    // well known properties ...
    static constexpr const char* wpHideChilds      = "HideChilds";		///< identifier well known property hide childs
    static constexpr const char* wpTreeItemEditor  = "TreeItemEditor";	///< identifier well known property tree item editor
    static constexpr const char* wpCaption         = "Caption";			///< identifier well known property caption
    static constexpr const char* wpHint            = "Hint";				///< identifier well known property hint

    // flags
    static constexpr uint32_t flagRd         = 0x00000001;		///< value is readable
    static constexpr uint32_t flagWr         = 0x00000002;		///< value is writable

public:
	/**
	 * Constructor.
	 * 
	 * \param name
	 * name of value
	 */
	ValueBase(std::string name);
    ~ValueBase();

	/**
	 * Get the VALUE_TYPE.
	 * \return
	 * VALUE_TYPE 
	 */
    template<typename T>
    static VALUE_TYPE whichType() {
        if (std::is_same<T, int8_t>::value) {
            return VALUE_TYPE::INT8;
        }
        if (std::is_same<T, uint8_t>::value) {
            return VALUE_TYPE::UINT8;
        }
        if (std::is_same<T, int16_t>::value) {
            return VALUE_TYPE::INT16;
        }
        if (std::is_same<T, uint16_t>::value) {
            return VALUE_TYPE::UINT16;
        }
        if (std::is_same<T, int32_t>::value) {
            return VALUE_TYPE::INT32;
        }
        if (std::is_same<T, uint32_t>::value) {
            return VALUE_TYPE::UINT32;
        }
        if (std::is_same<T, int64_t>::value) {
            return VALUE_TYPE::INT64;
        }
        if (std::is_same<T, uint64_t>::value) {
            return VALUE_TYPE::UINT64;
        }
        if (std::is_same<T, bool>::value) {
            return VALUE_TYPE::BOOL;
        }
        if (std::is_same<T, float>::value) {
            return VALUE_TYPE::FLOAT;
        }
        if (std::is_same<T, double>::value) {
            return VALUE_TYPE::DOUBLE;
        }
        if (std::is_same<T, std::string>::value) {
            return VALUE_TYPE::STRING;
        }
        
        return VALUE_TYPE::INVALID;
    }

	/**
	 * Get the number of childs.
	 * \return 
	 * number of childs
	 */
    size_t getChildCount()							{ return m_child.size();            }

	/**
	 * Get child.
	 * \param child
	 * number of child
	 * \return 
	 * shared pointer to child
	 */
    PTR_T getChild(const int child)					{ return m_child[child];            }

	/**
	 * Get Type.
	 * \return 
	 * VALUE_TYPE
	 */
    VALUE_TYPE& getType()							{ return m_valType;                 }

	/**
	 * Set if a child can be added repeatedly.
	 * \param v
	 * true: a child can only be added once
	 */
    void checkDuplicateEntries(bool v)				{ m_checkDuplicateEntries = v;      }

	/**
	 * Get volatile status.
	 * A volatile value is not stored/loaded to/from an xml file.
	 * \return 
	 * true: value is volatile
	 */
    bool isVolatile()								{ return m_volatile;                }

	/**
	 * Set volatile status.
	 * A volatile value is not stored/loaded to/from an xml file.
	 * \param v
	 * new volatile status
	 */
    void setVolatile(bool v)						{ m_volatile = v;                   }

	/**
	 * Get value type.
	 * \return
	 * VALUE_TYPE
	 */
    VALUE_TYPE getValueType()						{ return m_valType;                 }

	/**
	 * Get value name.
	 * \return 
	 * value name
	 */
    const std::string& getValueName()				{ return m_valName;                 }

	/**
	 * Get type name.
	 * \return 
	 * type name
	 */
    const std::string& getTypeName()				{ return m_typeNameMap[m_valType];  }

	/**
	 * Get flags.
	 * \return 
	 * flags
	 */
    uint32_t getFlags()								{ return m_flags;                   }

	/**
	 * Set flags.
	 * \param flags
	 * flags to be set
	 */
    void setFlags(const uint32_t flags)				{ m_flags |= flags;                 }

	/**
	 * Clear flags.
	 * \param flags
	 * flags to be cleared
	 */
    void clrFlags(const uint32_t flags)				{ m_flags &= ~flags;                }

	/**
	 * Load an xml file.
	 * \param statusMsg
	 * contains a status message
	 * \param fname
	 * the name of the file to be loaded
	 * \param node
	 * the entry node
	 * \return 
	 * true: success, false: error, look at the statusMsg
	 */
	static bool loadFromFile(std::string& statusMsg, std::string fname, ValueComplex* node);

	/**
	 * Store node and node childs to an xml file.
	 * \param rootName
	 * root name ???
	 * \param fname
	 * destination file nane
	 * \param node
	 * the entry node
	 * \return 
	 * true: success, false: erro
	 */
	static bool saveToFile(const std::string& rootName, std::string fname, ValueComplex* node);

	/**
	 * Set the parent.
	 */
	void setParent(const PTR_T& p);

	/**
	 * Get parent.
	 * \return 
	 * Shared pointer to parent.
	 */
    PTR_T getParent();

	/**
	 * Lock access to value.
	 * \return 
	 * true: access to value could be locked, false: access to value could not be locked.
	 */
    bool lockAccess()   { return m_accessLock.try_lock();   }

	/**
	 * Unlock access to value.
	 */
    void unlock()       { m_accessLock.unlock();            }

	/**
	 * Call "onValueChanged".
	 */
    void valueChanged() { onValueChanged(); }

	/**
	 * Get the number of listener.
	 * \return 
	 * number of listener.
	 */
    int listener();

	/**
	 * Set or add a property.
	 * \param name
	 * the name of the property
	 * \param value
	 * the value of the property
	 */
    template<typename T>
    void setProperty(const char* name, const T value) 
    {
        const std::lock_guard<std::recursive_mutex> lock(m_accessLock);
        PropertyContainer variant;
   
        variant = value;
        m_propertys[name] = variant;
    }

	/**
	 * Set or add a property.
	 * \param name
	 * the name of the property
	 * \param value
	 * the value of the property
	 */
    void setProperty(const char* name, char* value)
    {
		setProperty(name, static_cast<const char*>(value));
    }

	/**
	 * Set or add a property.
	 * \param name
	 * the name of the property
	 * \param value
	 * the value of the property
	 */
    void setProperty(const char* name, const char* value)
    {
        const std::lock_guard<std::recursive_mutex> lock(m_accessLock);
        std::string strContainer(value);
        PropertyContainer variant;

        variant = strContainer;
        m_propertys[name] = variant;
    }

	/**
	 * Get a property.
	 * \param name
	 * the name of the property
	 * \param value
	 * if the property is known, the value of the property is assigne to dest 
	 * \return 
	 * true: property is known, false: property is unknown
	 */
    template<typename T>
    bool getProperty(const char* name, T& dest)
    {
        const std::lock_guard<std::recursive_mutex> lock(m_accessLock);
        auto pos = m_propertys.find(name);

        if (pos == m_propertys.end()) {
            return false;
        }
        dest = std::get<T>(pos->second);
        return true;
    }

	/**
	 * Add a value change callback reference.
	 * \param client
	 * callback reference
	 */
    void addValueChangedCallback(CB_PTR client);

	/**
	 * Remove a value change callback reference.
	 * \param client
	 * callback reference
	 */
    void removeValueChangedCallback(CB_PTR client);

	/**
	 * Add a child added callback reference
	 * 
	 * \param client
	 * callback reference
	 */
    void addChildAddedCallback(const std::function<void(PTR_T, PTR_T)>* client);

	/**
	* Remove a child added callback reference
	*
	* \param client
	* callback reference
	*/
    void removeChildAddedCallback(const std::function<void(PTR_T, PTR_T)>* client);

	/**
	 * Write properties.
	 * 
	 * \param node
	 * \return 
	 * true: success, false: error
	 */
    bool writeProperties(pugi::xml_node& node);

	/**
	 * Read properties.
	 * 
	 * \param node
	 * \return 
	 * true: success, false: error
	 */
    bool readProperties(const pugi::xml_node& node);

	/**
	 * Get the path of the node.
	 * \param pathName
	 * complete path with parents
	 */
	void path(std::string& pathName);

	/**
	 * Get child identified by name.
	 * The name might be a complete path.
	 * 
	 * \param path
	 * path to child
	 * \return 
	 * shared pointer to child, may be NULL if the child
	 * is unknown 
	 */
    PTR_T getChild(const std::string& path);

	/**
	 * Get descendant identified by name.
	 * The name might be a complete path.
	 * 
	 * \param name
	 * path to child
	 * \return 
	 * shared pointer to child
	 */
	ValueBase::PTR_T getDescendant(const std::string& name);

	/**
	 * Check if type of object equals type of t.
	 * \param obj
	 * object to compare against
	 * \param t
	 * VALUE_TYPE to compare against
	 * \return 
	 * true: obj::VALUE_TYPE equals t::VALUE_TYPE, false: equals not
	 */
	bool checkType(PTR_T obj, VALUE_TYPE t) { 
		if (obj == nullptr) {
			return false;
		}
		return obj->getType() == t;
	}

	/**
	 * Get value of a child.
	 * \param val
	 * the value of child is assigned to val if child is known. Of course, the type has also to match.
	 * \param name
	 * the name or path to the child
	 * \return 
	 * true, if value of child could be assigned to val, otherwise false.
	 */
	template<typename T>
	bool getChildValue(T& val, const std::string name) {
		ValueBase::PTR_T ref = getChild(name);
		if (!ref)
			return false;
		if (ref->getType() != whichType<T>())
			return false;
		if (ref->lockAccess()) {
			val = std::get<T>(ref->getContainer());
			ref->unlock();
			return true;
		}
		return false;
	}

	/**
	 * Get value of a child.
	 * \param val
	 * the value of child is assigned to val if child is known. Of course, the type has also to match.
	 * \param idx
	 * the idx of the child
	 * \return
	 * true, if value of child could be assigned to val, otherwise false.
	 */
	template<typename T>
	bool getChildValue(T& val, int idx) {
		ValueBase::PTR_T ref = getChild(idx);

		if (!ref) {
			return false;
		}
		if (whichType<T>() != ref->getType()) {
			return false;
		}
		if (ref->lockAccess()) {
			val = std::get<T>(ref->getContainer());
			ref->unlock();
			return true;
		}
		return false;
	}

	/**
	 * Set new value of a child.
	 * \param val
	 * the new value for the child
	 * \param idx
	 * the index of the child
	 * \return 
	 * true: value could be assigned to child, otherwise false
	 */
	template<typename T>
	bool setChildValue(const T val, int idx) {
		ValueBase::PTR_T ref = getChild(idx);

		if (!ref) {
			return false;
		}
		if (whichType<T>() != ref->getType()) {
			return false;
		}
		if (ref->lockAccess()) {
			T shadow = std::get<T>(ref->getContainer());
			if (shadow != val) {
				if (m_maxContainer != nullptr) {
					if (std::get<T>(*m_maxContainer) < val) {
						return false;
					}
				}
				if (m_minContainer != nullptr) {
					if (std::get<T>(*m_minContainer) > val) {
						return false;
					}
				}
				ref->setContainer() = val;
				onValueChanged();
			}
			ref->unlock();
			return true;
		}
		return false;
	}

	/**
	 * Set new value of a child.
	 * \param val
	 * new value of child
	 * \param name
	 * name/path from/to child 
	 * \return 
	 * true: value could be assigned
	 */
	template<typename T>
	bool setChildValue(const T val, const std::string name) {
		ValueBase::PTR_T ref = getChild(name);

		if (!ref) {
			return false;
		}
		if (whichType<T>() != ref->getType()) {
			return false;
		}
		if (ref->lockAccess()) {
			T shadow = std::get<T>(ref->getContainer());
			if (shadow != val) {
				if (m_maxContainer != nullptr) {
					if ( std::get<T>(*m_maxContainer) < val) {
						return false;
					}
				}
				if (m_minContainer != nullptr) {
					if (std::get<T>(*m_minContainer) > val) {
						return false;
					}
				}
				ref->setContainer() = val;
				onValueChanged();
			}
			ref->unlock();
			return true;
		}
		return false;
	}


	/**
	 * Get vector of childs.
	 * \return 
	 * vector of childs
	 */
	const std::vector<PTR_T> getChilds() { return m_child; }

    virtual void addHash(ValueHash& hash) = 0;
    
    virtual bool write(pugi::xml_node& node) = 0;
    virtual bool read(pugi::xml_node& node) = 0;

    virtual void setValueSize(pugi::xml_node& node, int size);
    virtual bool getValueSize(pugi::xml_node& node, int& valueSize);

    virtual bool addChild(PTR_T child);
	bool removeChild(ValueBase* child);

    virtual void removeChilds(void);

    virtual bool copy(PTR_T src)		{ return false; }
    virtual bool compare(PTR_T src)		{ return false; }

    virtual bool hasRange()				{ return false; }

protected:

    void setValueType(pugi::xml_node& node);
    void getValueType(pugi::xml_node& node, VALUE_TYPE& t);

    void onDestroyed();
    void onValueChanged();
    
    void onChildAdded(PTR_T child);

    void* operator new(size_t sz);
};
