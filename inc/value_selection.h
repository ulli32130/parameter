#pragma once

#include "value_base.h"
#include "value.h"
#include "value_complex.h"


#include <vector>
#include <atomic>
#include <any>
#include <memory>

class PARAMETER_EXPORT ValueSelection : public ValueComplex {

public:
	typedef std::shared_ptr<ValueSelection>							PTR_T;		///< shared pointer to ValueSelection
	typedef std::function<void(PTR_T)>								CBREF_T;	///< callback function
	typedef const CBREF_T*											CB_PTR;		///< reference to callback function

	class CallbackMonitorItemBase
	{
	public:
		virtual ~CallbackMonitorItemBase() { ; }
	};

	class CallbackMonitorItem : public CallbackMonitorItemBase
	{
	private:
		ValueSelection::PTR_T						m_sender;
		std::function<void(ValueSelection::PTR_T)>	m_cbRef;

	public:
		CallbackMonitorItem(const ValueSelection::PTR_T& sender, std::function<void(ValueSelection::PTR_T)> cb)
			: m_sender(sender)
			, m_cbRef(cb)
		{
			m_sender->addListener(&m_cbRef);
		}

		virtual ~CallbackMonitorItem()
		{
			m_sender->removeListener(&m_cbRef);
		}
	};

	class CallbackMonitor
	{
	private:
		std::vector<CallbackMonitorItemBase*>	m_itemList;

	public:
		~CallbackMonitor()
		{
			for (const auto& item : m_itemList)
			{
				delete item;
			}
		}

		void connect(const ValueSelection::PTR_T& sender, std::function<void(ValueSelection::PTR_T)> cb)
		{
			m_itemList.push_back(new CallbackMonitorItem(sender, cb));
		}
	};

private:

#pragma warning(push)
#pragma warning( disable: 4251 )

    std::atomic<int>												m_idxSelectedItem;
    std::list<const std::function<void(ValueSelection::PTR_T)>*>	m_selectableListChangedListener;
#pragma warning( pop )

    bool															m_selectableItemsVolatile;

    const char* idItemType              = "ItemType";
    const char* idItemCount             = "ItemCount";
    const char* idSelectedItem          = "SelectedItem";


public:
	/**
	 * Constructor.
	 * \param name
	 * name of node
	 */
	ValueSelection(std::string name);
    ~ValueSelection() override;
   
	/**
	 * Create a new item if the parent has no child with name <name> or take the existing item
	 * from the parent.
	 * \param parent
	 * reference to parent, my be null if item is orphaned
	 * \param name
	 * item name
	 * \return
	 * shared pointer to item
	 */
	static PTR_T create(ValueBase::PTR_T parent, std::string name);

	/**
	 * Create a new item without parent
	 * \param name
	 * item name
	 * \return
	 * shared pointer to item
	 */
	static PTR_T create(std::string name);

	/**
	 * Set selectable items to volatile. If true, this items are not
	 * stored in the xml file.
	 * 
	 * \param v
	 * true: selectable items are not stored in the xml file, false: selectable 
	 * items are stored in the xml file 
	 */
    void setSelectableItemsVolatile(bool v)                 { m_selectableItemsVolatile = v; }

	/**
	 * Get a vector with std::string of selectable items.
	 * 
	 * \return 
	 * vector with std::string of selectable items
	 */
    std::vector<std::string> getItems();

	/**
	 * Clear list of selectable items.
	 * 
	 */
    void clearList();

	/**
	 * Set new list with selectable items.
	 * 
	 * \param list
	 * list with selectable items
	 * \param append
	 * false: the old list is cleared, true: new items are appended to the old list
	 */
	void setList(const std::list<ValueBase::PTR_T>& list, bool append = false);

	/**
	 * Append vector with selectable items to the current list.
	 * \param list
	 * vector with selectable items 
	 * \param itemsAdded
	 * true:  items were added, false: no items were added 
	 * (because the items in the vector are
	 * already in the current list.
	 */
    void appendSelectableItems(std::vector<std::shared_ptr<ValueBase>>& list, bool& itemsAdded);

	/**
	 * Set item.
	 * \param val
	 * shared pointer to item which has to be selected
	 * \return 
	 * true, if succeeded, else false.
	 */
    bool setSelected(const ValueBase::PTR_T& val);

	/**
	 * Set selected iitem ba index.
	 * \param value
	 * index number
	 * \param emitChange
	 * true: change handler are called if selceted item changes, 
	 * else not. 
	 * \return 
	 * false: index out of range
	 */
    bool setSelectedIdx(int value, bool emitChange = true);

	/**
	 * Return index of selected item.
	 * \return 
	 * -1 => no item selected, else index 
	 * of selected item. 
	 */
    int getSelectedIdx();

	/**
	 * Set selected item wich has a match to value.
	 * 
	 * \param value
	 * \param emitChange
	 * true: change handler are called if selceted item changes,
	 * else not.
	 * \return 
	 * false: no match found, else true
	 */
    template<typename T>
    bool setSelectedValue(T value, bool emitChange = true)
    {
        const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

        VALUE_TYPE vt = whichType<T>();

        for (int n = 0; n < m_child.size(); n++) {
            // check if type matches ...
            if (m_child[n]->getValueType() == vt) {
                // check if value matches ...
				if (value == Value<T>::cast(m_child[n])->get()) {
                    // set index
                    setSelectedIdx(n, emitChange);
                    return true;
                }
            }
        }
        return false;
    }

	/**
	 * Get selected value.
	 * \return 
	 * std::any of selected item
	 */
    template<typename T>
    std::any getSelectedValue()
    {
        const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

        ValueBase::PTR_T item = getItem();

        if (item == NULL) {
            return std::any();
        }

        if (ValueBase::whichType<T>() != item->getType()) {
            return std::any();
        }

		return Value<T>::cast(item)->get();
    }

	/**
	 * Assigns value os selected item to <val>.
	 * \param val
	 * 
	 * \return 
	 * false: no item selected, else true
	 */
    template<typename T>
    bool getSelectedValue(T& val)
    {
        std::any anyVal = getSelectedValue<T>();
        if (anyVal.has_value()) {
            val = std::any_cast<T>(anyVal);
            return true;
        }
        return false;
    }

	/**
	 * Get selected item.
	 * \return 
	 * ValueBase::PTR_T of selected item.
	 */
	ValueBase::PTR_T getSelected();

    void onSelectableItemListChanged();

	/**
	 * Add callback reference to the list of callback handler that are
	 * to be called if the list of selectable items changes.
	 * \param watcher
	 * the callback reference
	 */
	void addListWatcher(CB_PTR watcher);

	/**
	 * Remove callback reference from the list of callback handler that are
	 * to be called if the list of selectable items changes.
	 * \param watcher
	 * the callback reference
	 */
	void removeListWatcher(CB_PTR watcher);

	/**
	 * Add callback reference to the list of callback handler that are
	 * to be called if the selected item changes.
	 * \param client
	 * the callback reference
	 */
    void addListener(CB_PTR client);

	/**
	 * Remove callback reference to the list of callback handler that are
	 * to be called if the selected item changes.
	 * \param client
	 * the callback reference
	 */
    void removeListener(CB_PTR client);

    bool write(pugi::xml_node& parent)                            override;
    bool read(pugi::xml_node& parent)                             override;

    void addHash(ValueHash& hash)                                 override;

	static inline std::shared_ptr<ValueSelection> cast(std::shared_ptr<ValueBase>& base) 
	{
		return std::dynamic_pointer_cast<ValueSelection>(base);
	}

private:
	std::shared_ptr<ValueBase>  getItem();
};
