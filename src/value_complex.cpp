#include "value_complex.h"
#include "value_selection.h"
#include "value.h"

#include <algorithm>


ValueComplex::ValueComplex(const std::string name) :
	ValueBase(name)
{
	m_valType = VALUE_TYPE::COMPLEX;
}

ValueComplex::~ValueComplex()
= default;

bool ValueComplex::write(pugi::xml_node& parent)
{
	pugi::xml_node nodeChild = parent.append_child(m_typeNameMap[m_valType].c_str());

	nodeChild.append_attribute(idName)	= m_valName.c_str();
	nodeChild.append_attribute(idType)	= static_cast<int>(m_valType);
	nodeChild.append_attribute(idItems) = static_cast<int>(m_child.size());

	writeProperties(nodeChild);

	for (auto it : m_child) {
		it->write(nodeChild);
	}

	return true;
}

bool ValueComplex::read(pugi::xml_node& node)
{
	if (node.name() == m_typeNameMap[m_valType]) {
		// read childs
		for (pugi::xml_node child : node.children())
		{
			bool childRead = false;

			// walk to list of known nodes
			for (auto entry : m_child) {

				if (0 == strcmp(child.name(), idProperties)) {
					readProperties(node);
					childRead = true;
					break;
				}

				pugi::xml_attribute attr = child.attribute(idName);

				if (attr == nullptr) {
					return false;
				}

				if (attr.as_string() == entry->getValueName()) {
					//printf("read child : %s\n", attr.as_string());
					if (!entry->read(child)) {
						printf("read failed ??\n");
						return false;
					}
					childRead = true;
					break;
				}
			}

			// we don't know this node this node until now, create a new entry ...
			if (!childRead) {
				readChild(shared_from_this(), child);
			}
		}
	}

	return true;
}

ValueBase::PTR_T ValueComplex::readChild(ValueBase::PTR_T parent, pugi::xml_node& node)
{
	ValueBase::PTR_T value;

	// read node properties if present
	if (0 == strcmp(node.name(), idProperties)) {
		for (auto property : node.children(idProperty)) {

			auto attrType = property.attribute(idType);
			int valType = attrType.as_uint();
			auto attrValue = property.attribute(idValue);
			auto attrName = property.attribute(idName);

			switch (valType) {
			case 0: // int8_t
				parent->setProperty<int8_t>(attrName.as_string(), attrValue.as_int());
				break;

			case 1: // uint8_t
				parent->setProperty<uint8_t>(attrName.as_string(), attrValue.as_uint());
				break;

			case 2: // int16_t
				parent->setProperty<int16_t>(attrName.as_string(), attrValue.as_int());
				break;

			case 3: // uint16_t  
				parent->setProperty<uint16_t>(attrName.as_string(), attrValue.as_uint());
				break;

			case 4: // int32_t 
				parent->setProperty<int32_t>(attrName.as_string(), attrValue.as_int());
				break;

			case 5:	// uint32_t 
				parent->setProperty<uint32_t>(attrName.as_string(), attrValue.as_uint());
				break;

			case 6:	// int64_t
				parent->setProperty<int64_t>(attrName.as_string(), attrValue.as_llong());
				break;

			case 7:	// uint64_t
				parent->setProperty<uint64_t>(attrName.as_string(), attrValue.as_ullong());
				break;

			case 8:	// bool 
				parent->setProperty<bool>(attrName.as_string(), attrValue.as_bool());
				break;

			case 9: // float
				parent->setProperty<float>(attrName.as_string(), attrValue.as_float());
				break;

			case 10: // double
				parent->setProperty<double>(attrName.as_string(), attrValue.as_double());
				break;

			case 11: // std::string
				parent->setProperty<std::string>(attrName.as_string(), attrValue.as_string());
				break;

			default:

				break;
			}
		}
		return nullptr;
	}

	pugi::xml_attribute attrType = node.attribute(idType);
	if (attrType == nullptr) {
		return nullptr;
	}

	pugi::xml_attribute attrName = node.attribute(idName);
	if (attrType == nullptr) {
		return nullptr;
	}

	auto type = static_cast<VALUE_TYPE>(attrType.as_int());

	switch (type) {
	case VALUE_TYPE::UINT8:
		value = Value<uint8_t>::create(parent, attrName.as_string(), 0);
		value->read(node);
		return value;

	case VALUE_TYPE::INT8:
		value = Value<int8_t>::create(parent, attrName.as_string(), 0);
		value->read(node);
		return value;

	case VALUE_TYPE::UINT16:
		value = Value<uint16_t>::create(parent, attrName.as_string(), 0);
		value->read(node);
		return value;

	case VALUE_TYPE::INT16:
		value = Value<int16_t>::create(parent, attrName.as_string(), 0);
		value->read(node);
		return value;

	case VALUE_TYPE::INT32:
		value = Value<int32_t>::create(parent, attrName.as_string(), 0);
        value->read(node);
		return value;

	case VALUE_TYPE::UINT32:
		value = Value<uint32_t>::create(parent, attrName.as_string(), 0);
        value->read(node);
		return value;

	case VALUE_TYPE::INT64:
		value = Value<int64_t>::create(parent, attrName.as_string(), 0);
		value->read(node);
		return value;

	case VALUE_TYPE::UINT64:
		value = Value<uint64_t>::create(parent, attrName.as_string(), 0);
		value->read(node);
		return value;

	case VALUE_TYPE::FLOAT:
		value = Value<float>::create(parent, attrName.as_string(), 0);
        value->read(node);
		return value;

	case VALUE_TYPE::DOUBLE:
		value = Value<double>::create(parent, attrName.as_string(), 0);
        value->read(node);
		return value;

	case VALUE_TYPE::BOOL:
		value = Value<bool>::create(parent, attrName.as_string(), 0);
		value->read(node);
		return value;

	case VALUE_TYPE::STRING:
		value = Value<std::string>::create(parent, attrName.as_string(), "");
		value->read(node);
		return value;
	
	case VALUE_TYPE::COMPLEX:
		value = ValueComplex::create(parent, attrName.as_string());
		value->read(node);
		return value;

	case VALUE_TYPE::SELECTION:
		value = ValueSelection::create(parent, attrName.as_string());
		value->read(node);
		return value;

	default:
		break;
	}

	return nullptr;
}

ValueComplex::PTR_T ValueComplex::create(ValueBase::PTR_T parent, std::string name)
{
	ValueBase::PTR_T child = parent->getChild(name);

	if (child) {
		if (child->getType() != VALUE_TYPE::COMPLEX) {
			// the types don't match ....
			return nullptr;
		}

		PTR_T p = ValueComplex::cast(child);
		return p;
	}

	PTR_T p = std::make_shared<ValueComplex>(name);

	parent->addChild(p);
	p->setParent(parent);

	return p;
}

ValueComplex::PTR_T ValueComplex::create(std::string name)
{
	return std::make_shared<ValueComplex>(name);
}

void ValueComplex::addHash(ValueHash& hash)
{
	for (const auto& it : m_child) {
		it->addHash(hash);
	}
}








