#include "value_selection.h"
#include "value.h"
#include "value_complex.h"
#include <memory>
#include <algorithm>

ValueSelection::ValueSelection(const std::string name)
	: ValueComplex(name)
	, m_idxSelectedItem(-1)
	, m_selectableItemsVolatile(false)
{
	m_valType = VALUE_TYPE::SELECTION;
}

ValueSelection::~ValueSelection()
= default;

std::vector<std::string> ValueSelection::getItems()
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	std::vector<std::string> list;

	for (const auto& item : m_child) {
		list.push_back(item->getValueName());
	}
	return list;
}

void ValueSelection::clearList()
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	for (auto item : m_child) {
		item->setParent(nullptr);
		item.reset();
	}
	m_child.clear();
}

void ValueSelection::setList(const std::list<ValueBase::PTR_T>& list, bool append)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	ValueBase::PTR_T p = getSelected();

	if (!append) {
		clearList();
	}

	for (const auto& item : list) {
		item->setParent(shared_from_this());
		addChild(item);
	}

	onSelectableItemListChanged();

	if (p != nullptr) {
		setSelected(p);
		onValueChanged();
	}
}

int ValueSelection::getSelectedIdx()
{
	return m_idxSelectedItem;
}

bool ValueSelection::setSelected(const ValueBase::PTR_T& val)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	for (int n = 0; n < m_child.size(); n++) {
		if (m_child[n]->compare(val)) {
			setSelectedIdx(n);
			return true;
		}
	}
	return false;
}

bool ValueSelection::setSelectedIdx(int value, bool emitChange)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);
	
	int newVal;

	if (value == -1) {
		newVal = -1;
	}
	else {
		if (value > m_child.size()) {
			return false;
		}
		newVal = value;
	}

	if (m_idxSelectedItem == newVal) {
		return true;
	}
	
	m_idxSelectedItem = newVal;

	if(emitChange)
		onValueChanged();
	return true;
}

void ValueSelection::onSelectableItemListChanged()
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	for (auto  it : m_selectableListChangedListener) {
		(*it)(std::dynamic_pointer_cast<ValueSelection>(shared_from_this()));
	}
}

void ValueSelection::addListWatcher(CB_PTR watcher)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	m_selectableListChangedListener.push_back(watcher);
}

void ValueSelection::removeListWatcher(CB_PTR watcher)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	m_selectableListChangedListener.remove(watcher);
}

void ValueSelection::addListener(CB_PTR client)
{
	ValueBase::addValueChangedCallback(reinterpret_cast<const std::function<void(std::shared_ptr<ValueBase>)>*>(client));
}

void ValueSelection::removeListener(CB_PTR client)
{
	ValueBase::removeValueChangedCallback(reinterpret_cast<const std::function<void(std::shared_ptr<ValueBase>)>*>(client));
}

bool ValueSelection::write(pugi::xml_node& parent)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	pugi::xml_node nodeChild = parent.append_child(m_typeNameMap[m_valType].c_str());

	writeProperties(nodeChild);

	setValueType(nodeChild);

	nodeChild.append_attribute(idName)	= m_valName.c_str();
	nodeChild.append_attribute(idType)	= static_cast<int>(m_valType);
	nodeChild.append_attribute(idItems) = static_cast<int>(m_child.size());

	if (!m_selectableItemsVolatile) {
		//########################## write selectable items ...
		pugi::xml_node nodeItems = nodeChild.append_child(m_typeNameMap[VALUE_TYPE::ITEM_LIST].c_str());
		for (auto item : m_child) {
			item->write(nodeItems);
			item->writeProperties(nodeItems);
		}
	}

	//########################## write selected item
	pugi::xml_node nodeSelectedItem = nodeChild.append_child(m_typeNameMap[VALUE_TYPE::SELECTED_ITEM].c_str());

	if (0 <= m_idxSelectedItem && m_idxSelectedItem < m_child.size()) {
		m_child[m_idxSelectedItem]->write(nodeSelectedItem);
	}

	return true;
}

void ValueSelection::appendSelectableItems(std::vector<std::shared_ptr<ValueBase>>& list, bool& itemsAdded)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	itemsAdded = false;

	while (list.size()) {

		auto itStart = list.begin();

		std::shared_ptr<ValueBase> p = list[list.size() - 1];
		list.pop_back();

		for(auto knownItem : m_child) {
			if (p->getValueName() == knownItem->getValueName()) {
				p.reset();
				p = nullptr;
				break;
			}
		}
		if (p != nullptr) {
			p->setParent(shared_from_this());
			m_child.emplace_back(p);
			itemsAdded = true;
		}
	}
}

bool ValueSelection::read(pugi::xml_node& parent)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	std::vector<std::shared_ptr<ValueBase>>		itemList;
	std::shared_ptr<ValueBase>					selectedValue;

	readProperties(parent);
	// read selectable items
	pugi::xml_node childNode = parent.child(m_typeNameMap[VALUE_TYPE::ITEM_LIST].c_str());

	if (!m_selectableItemsVolatile) {
		for (auto item : childNode.children()) {
			// generate child
			itemList.push_back(readChild(nullptr, item));
			// read properties from child 
			readProperties(childNode);
		}

		bool itemsAdded;

		appendSelectableItems(itemList, itemsAdded);
		if (itemsAdded) {
			onSelectableItemListChanged();
		}
	}

	// read selected item, if avaiable 
	childNode = parent.child(m_typeNameMap[VALUE_TYPE::SELECTED_ITEM].c_str());

	// should only have one child ...
	pugi::xml_node selectedItemNode = childNode.first_child();
	if (selectedItemNode != nullptr) {
		selectedValue = readChild(nullptr, selectedItemNode);

		bool valChanged = !selectedValue->compare(getSelected());

		if (valChanged) {
			setSelected(selectedValue);
			onValueChanged();
		}
		selectedValue.reset();
	}

	return true;
}

ValueBase::PTR_T ValueSelection::getSelected()
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	if (m_idxSelectedItem < 0 || m_idxSelectedItem >= m_child.size()) {
		return std::shared_ptr<ValueBase>(nullptr);
	}
	return m_child[m_idxSelectedItem];
}

void ValueSelection::addHash(ValueHash& hash)
{
	ValueComplex::addHash(hash);

	ValueBase::PTR_T selected = getSelected();
	if (selected != nullptr) {
		selected->addHash(hash);
	}
}

std::shared_ptr<ValueBase> ValueSelection::getItem()
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	if (m_idxSelectedItem < 0 || m_idxSelectedItem >= m_child.size()) {
		return nullptr;
	}
	return m_child[m_idxSelectedItem];
}

ValueSelection::PTR_T ValueSelection::create(ValueBase::PTR_T parent, std::string name)
{
	if (parent.operator->() == nullptr) {
		return create(name);
	}

	ValueBase::PTR_T child = parent->getChild(name);

	if (child != nullptr) {
		if (child->getType() != VALUE_TYPE::SELECTION) {
			// the types don't match ....
			return nullptr;
		}

		PTR_T p = ValueSelection::cast(child);
		return p;
	}

	PTR_T p = std::make_shared<ValueSelection>(name);
	parent->addChild(p);
	p.operator->()->setParent(parent);
	return p;
}

ValueSelection::PTR_T ValueSelection::create(std::string name)
{
	return std::make_shared<ValueSelection>(name);
}