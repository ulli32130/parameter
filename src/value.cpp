#include "value.h"
#include "simple_base64.h"

#ifdef DYNAMIC_LIB
#include "parameter_export.h"
#else    
#define PARAMETER_EXPORT
#endif    

template<>
void PARAMETER_EXPORT Value<int8_t>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	int8_t val = std::get<int8_t>(m_container);

	hash.add(&val, sizeof(int8_t));
}

template<>
void PARAMETER_EXPORT Value<uint8_t>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	uint8_t val = std::get<uint8_t>(m_container);
	hash.add(&val, sizeof(uint8_t));
}

template<>
void PARAMETER_EXPORT Value<int16_t>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	int16_t val = std::get<int16_t>(m_container);
	hash.add(&val, sizeof(int16_t));
}

template<>
void PARAMETER_EXPORT Value<uint16_t>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	uint16_t val = std::get<uint16_t>(m_container);
	hash.add(&val, sizeof(uint16_t));
}

template<>
void PARAMETER_EXPORT Value<int32_t>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	uint32_t val = std::get<int32_t>(m_container);
	hash.add(&val, sizeof(int32_t));
}

template<>
void PARAMETER_EXPORT Value<uint32_t>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	uint32_t val = std::get<uint32_t>(m_container);
	hash.add(&val, sizeof(uint32_t));
}

template<>
void PARAMETER_EXPORT Value<int64_t>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	int64_t val = std::get<int64_t>(m_container);
	hash.add(&val, sizeof(int64_t));
}

template<>
void PARAMETER_EXPORT Value<uint64_t>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	uint64_t val = std::get<uint64_t>(m_container);
	hash.add(&val, sizeof(uint64_t));
}

template<>
void PARAMETER_EXPORT Value<double>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	double val = std::get<double>(m_container);
	hash.add(&val, sizeof(double));
}

template<>
void PARAMETER_EXPORT Value<float>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	float val = std::get<float>(m_container);
	hash.add(&val, sizeof(float));
}

template<>
void PARAMETER_EXPORT Value<std::string>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	std::string val = std::get<std::string>(m_container);
	hash.add(val.c_str(), val.size());
}

template<>
void PARAMETER_EXPORT Value<bool>::addHash(ValueHash& hash)
{
	if (isVolatile()) {
		return;
	}
	bool val = std::get<bool>(m_container);
	hash.add(&val, sizeof(bool));
}

//#################################################################################################

template<>
void PARAMETER_EXPORT Value<int8_t>::writeValue(pugi::xml_node& node)
{
	node.append_attribute(idValue) = std::get<int8_t>(m_container);
}

template<>
void PARAMETER_EXPORT Value<uint8_t>::writeValue(pugi::xml_node& node)
{
	node.append_attribute(idValue) = std::get<uint8_t>(m_container);
}

template<>
void PARAMETER_EXPORT Value<int16_t>::writeValue(pugi::xml_node& node)
{
	node.append_attribute(idValue) = std::get<int16_t>(m_container);
}

template<>
void PARAMETER_EXPORT Value<uint16_t>::writeValue(pugi::xml_node& node)
{
	node.append_attribute(idValue) = std::get<uint16_t>(m_container);
}

template<>
void PARAMETER_EXPORT Value<int32_t>::writeValue(pugi::xml_node& node)
{
	node.append_attribute(idValue) = std::get<int32_t>(m_container);
}

template<>
void PARAMETER_EXPORT Value<uint32_t>::writeValue(pugi::xml_node& node)
{
	node.append_attribute(idValue) = std::get<uint32_t>(m_container);
}

template<>
void PARAMETER_EXPORT Value<int64_t>::writeValue(pugi::xml_node& node)
{
	node.append_attribute(idValue) = std::get<int64_t>(m_container);
}

template<>
void PARAMETER_EXPORT Value<uint64_t>::writeValue(pugi::xml_node& node)
{
	node.append_attribute(idValue) = std::get<uint64_t>(m_container);
}

template<>
void PARAMETER_EXPORT Value<float>::writeValue(pugi::xml_node& node)
{
	float value = std::get<float>(m_container);

	node.append_attribute(idValue)	= simpleBase64::base64_encode(reinterpret_cast<uint8_t*>(&value), sizeof(float)).c_str();
	node.append_attribute("hint")	= value;
}

template<>
void PARAMETER_EXPORT Value<double>::writeValue(pugi::xml_node& node)
{
	double value = std::get<double>(m_container);

	node.append_attribute(idValue) = simpleBase64::base64_encode(reinterpret_cast<uint8_t*>(&value), sizeof(double)).c_str();
	node.append_attribute("hint") = value;
}

template<>
void PARAMETER_EXPORT Value<bool>::writeValue(pugi::xml_node& node)
{
	node.append_attribute(idValue) = std::get<bool>(m_container);
}

template<>
void PARAMETER_EXPORT Value<std::string>::writeValue(pugi::xml_node& node)
{
	node.append_attribute(idValue) = std::get<std::string>(m_container).c_str();
}

template<>
void PARAMETER_EXPORT Value<int8_t>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	int8_t newVal = attr.as_int();
	valueChanged = (newVal != std::get<int8_t>(m_container));
	m_container = newVal;
}

template<>
void PARAMETER_EXPORT Value<uint8_t>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	uint8_t newVal = attr.as_uint();
	valueChanged = (newVal != std::get<uint8_t>(m_container));
	m_container = newVal;
}

template<>
void PARAMETER_EXPORT Value<int16_t>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	int16_t newVal = attr.as_int();
	valueChanged = (newVal != std::get<int16_t>(m_container));
	m_container = newVal;
}

template<>
void PARAMETER_EXPORT Value<uint16_t>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	uint16_t newVal = attr.as_uint();
	valueChanged = (newVal != std::get<uint16_t>(m_container));
	m_container = newVal;
}

template<>
void PARAMETER_EXPORT Value<int32_t>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	int32_t newVal = attr.as_int();
	valueChanged = (newVal != std::get<int32_t>(m_container));
	m_container = newVal;
}

template<>
void PARAMETER_EXPORT Value<uint32_t>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	uint32_t newVal = attr.as_uint();
	valueChanged = (newVal != std::get<uint32_t>(m_container));
	m_container = newVal;
}

template<>
void PARAMETER_EXPORT Value<int64_t>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	int64_t newVal = attr.as_llong();
	valueChanged = (newVal != std::get<int64_t>(m_container));
	m_container = newVal;
}

template<>
void PARAMETER_EXPORT Value<uint64_t>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	uint64_t newVal = attr.as_ullong();
	valueChanged = (newVal != std::get<uint64_t>(m_container));
	m_container = newVal;
}

template<>
void PARAMETER_EXPORT Value<float>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	std::string base64Value = attr.as_string();

	std::vector<unsigned char> ref = simpleBase64::base64_decode(base64Value);
	if (ref.size() != sizeof(float)) {
		// type missmatch
		return;
	}

	float newVal;
	memcpy(reinterpret_cast<char*>(&newVal), ref.data(), sizeof(float));

	valueChanged = (newVal != std::get<float>(m_container));
	m_container = newVal;
}

template<>
void PARAMETER_EXPORT Value<double>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	std::string base64Value = attr.as_string();

	std::vector<unsigned char> ref = simpleBase64::base64_decode(base64Value);
	if (ref.size() != sizeof(double)) {
		// size missmatch
		return;
	}

	double newVal;
	memcpy(reinterpret_cast<char*>(&newVal), ref.data(), sizeof(double));

	valueChanged = (newVal != std::get<double>(m_container));
	m_container = newVal;
}

template<>
void PARAMETER_EXPORT Value<bool>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	bool newVal = attr.as_bool();
	valueChanged = (newVal != std::get<bool>(m_container));
	m_container = newVal;
}

template<>
void PARAMETER_EXPORT Value<std::string>::readValue(pugi::xml_attribute& attr, bool& valueChanged)
{
	std::string newVal = attr.as_string();
	valueChanged = (newVal != std::get<std::string>(m_container));
	m_container = newVal;
}