#include "value_base.h"
#include "value_complex.h"


ValueBase::ValueBase(std::string name)
	: m_parent(nullptr)
	, m_valName(name)
	, m_volatile(false)
	, m_checkDuplicateEntries(true)
	, m_flags(flagRd | flagWr)
	, m_valType(VALUE_TYPE::INVALID)
	, m_minContainer(nullptr)
	, m_maxContainer(nullptr)
{
	
}

ValueBase::~ValueBase()
{
	if (m_parent != nullptr) {
		m_parent->removeChild(this);
	}

	for (auto item : m_child) {
		item->setParent(nullptr);
		item.reset();
	}

	m_child.clear();
}

void ValueBase::path(std::string& pathName)
{
	if (m_parent != nullptr) {
		m_parent->path(pathName);
	}
	if (pathName.length()) {
		pathName += '/';
	}
	pathName += m_valName;
}

bool ValueBase::loadFromFile(std::string& statusMsg, const std::string fname, ValueComplex* node)
{
	// Create empty XML document within memory
	pugi::xml_document doc;
	// Load XML file into memory
	// Remark: to fully read declaration entries you have to specify
	// "pugi::parse_declaration"

	pugi::xml_parse_result result = doc.load_file(fname.c_str(), pugi::parse_default | pugi::parse_declaration);

	if (!result)
	{
		char posMsg[256];
		snprintf(posMsg, sizeof(posMsg),
			"Parse error, %s , chracter pos= %lld",
			result.description(),
			static_cast<long long>(result.offset));

		statusMsg = std::string(posMsg);

		return false;
	}

	// A valid XML document must have a single root node
	pugi::xml_node root = doc.document_element();

	// must alwys start with one complex node
	pugi::xml_node params = root.child("COMPLEX");
	if (params == nullptr) {
		statusMsg = "COMPLEX entry node missing ...";
		return false;
	}

	node->read(params);

	return true;
}

bool ValueBase::saveToFile(const std::string& rootName, const std::string fname, ValueComplex* node)
{
	pugi::xml_document doc;
	// Alternatively store as shared pointer if tree shall be used for longer
	// time or multiple client calls:
	// std::shared_ptr<pugi::xml_document> spDoc = std::make_shared<pugi::xml_document>();
	// Generate XML declaration
	auto declarationNode = doc.append_child(pugi::node_declaration);
	declarationNode.append_attribute("version") = "1.0";
	declarationNode.append_attribute("encoding") = "ISO-8859-1";
	declarationNode.append_attribute("standalone") = "yes";
	// A valid XML doc must contain a single root node of any name
	auto root = doc.append_child(rootName.c_str());

	node->write(root);

	// Save XML tree to file.
	// Remark: second optional param is indent string to be used;
	// default indentation is tab character.

	bool saveSucceeded = doc.save_file(fname.c_str(), PUGIXML_TEXT("  "));

	return saveSucceeded;
}

int ValueBase::listener()
{
	const std::lock_guard<std::recursive_mutex>	lock(m_accessLock);

	return static_cast<int>(m_plistener.size());
}

void ValueBase::setParent(const ValueBase::PTR_T& p)
{
	const std::lock_guard<std::recursive_mutex>	lock(m_accessLock);

	m_parent = p;
}

std::shared_ptr<ValueBase> ValueBase::getParent()
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	return m_parent;
}

bool ValueBase::writeProperties(pugi::xml_node& node)
{
	if (m_propertys.empty()) {
		return true;
	}

	pugi::xml_node nodePropertys = node.append_child(idProperties);

	for (auto it : m_propertys) {
		pugi::xml_node prop = nodePropertys.append_child(idProperty);

		PropertyContainer container = it.second;

		prop.append_attribute(idName) = it.first.c_str();
		prop.append_attribute(idType) = static_cast<uint32_t>(container.index());

		switch (container.index()) {
		case 0: // int8_t
			prop.append_attribute(idValue) = std::get<int8_t>(container);
			break;

		case 1: // uint8_t
			prop.append_attribute(idValue) = std::get<uint8_t>(container);
			break;

		case 2: // int16_t
			prop.append_attribute(idValue) = std::get<int16_t>(container);
			break;
			
		case 3: // uint16_t  
			prop.append_attribute(idValue) = std::get<uint16_t>(container);
			break;

		case 4: // int32_t 
			prop.append_attribute(idValue) = std::get<int32_t>(container);
			break;

		case 5:	// uint32_t 
			prop.append_attribute(idValue) = std::get<uint32_t>(container);
			break;

		case 6:	// int64_t
			prop.append_attribute(idValue) = std::get<int64_t>(container);
			break;

		case 7:	// uint64_t
			prop.append_attribute(idValue) = std::get<uint64_t>(container);
			break;

		case 8:	// bool 
			prop.append_attribute(idValue) = std::get<bool>(container);
			break;

		case 9: // float
			prop.append_attribute(idValue) = std::get<float>(container);
			break;

		case 10: // double
			prop.append_attribute(idValue) = std::get<double>(container);
			break;

		case 11: // std::string
			prop.append_attribute(idValue) = std::get<std::string>(container).c_str();
			break;

		default:
	
			break;

		}

	}
	return true;
}

bool ValueBase::readProperties(const pugi::xml_node& node)
{
	const pugi::xml_node& properties = node.child(idProperties);

	for (auto property : properties.children(idProperty)) {

		auto attrType		= property.attribute(idType);
		int valType			= attrType.as_uint();
		auto attrValue		= property.attribute(idValue);
		auto attrName		= property.attribute(idName);

		switch (valType) {
		case 0: // int8_t
			setProperty<int8_t>(attrName.as_string(), attrValue.as_int());
			break;

		case 1: // uint8_t
			setProperty<uint8_t>(attrName.as_string(), attrValue.as_uint());
			break;

		case 2: // int16_t
			setProperty<int16_t>(attrName.as_string(), attrValue.as_int());
			break;

		case 3: // uint16_t  
			setProperty<uint16_t>(attrName.as_string(), attrValue.as_uint());
			break;

		case 4: // int32_t 
			setProperty<int32_t>(attrName.as_string(), attrValue.as_int());
			break;

		case 5:	// uint32_t 
			setProperty<uint32_t>(attrName.as_string(), attrValue.as_uint());
			break;

		case 6:	// int64_t
			setProperty<int64_t>(attrName.as_string(), attrValue.as_llong());
			break;

		case 7:	// uint64_t
			setProperty<uint64_t>(attrName.as_string(), attrValue.as_ullong());
			break;

		case 8:	// bool 
			setProperty<bool>(attrName.as_string(), attrValue.as_bool());
			break;

		case 9: // float
			setProperty<float>(attrName.as_string(), attrValue.as_float());
			break;

		case 10: // double
			setProperty<double>(attrName.as_string(), attrValue.as_double());
			break;

		case 11: // std::string
			setProperty<std::string>(attrName.as_string(), attrValue.as_string());
			break;

		default:

			break;
		}
	}
	return true;
}

void* ValueBase::operator new(size_t sz)
{
	return malloc(sz);
}

void ValueBase::addValueChangedCallback(CB_PTR client)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	for (auto item : m_plistener) {
		if (item == client) {
			// every callback can only be added once !
			return;
		}
	}

	m_plistener.push_back(client);
}

void ValueBase::removeValueChangedCallback(CB_PTR client)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	m_plistener.remove(client);
}

void ValueBase::getValueType(pugi::xml_node& node, VALUE_TYPE& t)
{

}

void ValueBase::setValueType(pugi::xml_node& node)
{
	node.append_attribute(idType) = static_cast<int>(m_valType);
}

void ValueBase::setValueSize(pugi::xml_node& node, const int size)
{
	node.append_attribute(idDataSize) = size;
}

bool ValueBase::getValueSize(pugi::xml_node& node, int& valueSize)
{
	return false;
}

void ValueBase::onValueChanged()
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	for (const auto & it : m_plistener) {
		it->operator()(shared_from_this());
	}
}

ValueBase::PTR_T ValueBase::getChild(const std::string& name)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	auto pos = name.find('/', 0);
	if (pos != std::string::npos) {
		ValueBase::PTR_T member = getChild(name.substr(0, pos));
		if (member) {
			return member->getChild(name.substr(pos + 1));
		}
	}
	else {
		for (auto& it : m_child) {
			if (it->getValueName() == name) {
				return it;
			}
		}
	}

	return nullptr;
}

struct entryNidx
{
	std::string parentName;
	ValueBase* parentEntry;
	uint32_t entryIdx;
};

ValueBase::PTR_T ValueBase::getDescendant(const std::string& paramName)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	ValueBase::PTR_T resultEntry = nullptr;

	entryNidx curOrder;
	std::vector<entryNidx> orderStack;

	ValueBase* parentEntry = this;

	uint32_t level = 0;
	std::string parentName = "";
	int32_t entryIdx = 0;
	ValueBase* entry;
	while (entryIdx < parentEntry->getChildCount())
	{
		// get the actual child
		ValueBase::PTR_T entry = parentEntry->getChild(entryIdx);

		std::string name;
		if (parentName.empty())
			name = entry->getValueName();
		else
			name = parentName + "/" + entry->getValueName();

		if (name == paramName)
		{
			resultEntry = entry;
			break;
		}

		// are there any children
		VALUE_TYPE type = entry->getType();
		if (type != VALUE_TYPE::SELECTION && type != VALUE_TYPE::SELECTED_ITEM && type != VALUE_TYPE::ITEM_LIST && entry->getChildCount() > 0)
		{
			// save the current order
			curOrder.parentName = parentName;
			curOrder.parentEntry = parentEntry;
			curOrder.entryIdx = entryIdx;

			orderStack.push_back(curOrder);

			// look a level lower
			level++;

			parentName = name;
			entryIdx = 0;
			parentEntry = entry.get();
			continue;
		}

		// move to the next child
		while (++entryIdx >= parentEntry->getChildCount())
		{	// if there is no next child

			// if not possible to move higher
			if (orderStack.size() == 0)
				goto nothing_found; // nothing found

			// move a level higher
			level--;

			curOrder = orderStack.back();
			orderStack.pop_back();

			parentName = curOrder.parentName;
			parentEntry = curOrder.parentEntry;
			entryIdx = curOrder.entryIdx;
		}
	};

nothing_found:

	return resultEntry;
}

bool ValueBase::addChild(std::shared_ptr<ValueBase> child)
{
	if (m_checkDuplicateEntries) {
		for (auto& item : m_child) {
			if (item->getValueName() == child->getValueName()) {
				return false;
			}
		}
	}

	m_child.push_back(child);
	onChildAdded(child);
	return true;
}

bool ValueBase::removeChild(ValueBase* child)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);
	std::vector<std::shared_ptr<ValueBase>>::iterator it;

	for (it = m_child.begin(); it != m_child.end(); it++) {
		if ( (*it).operator->() == child) {
			m_child.erase(it);
			return true;
		}
	}

	return false;
}

void ValueBase::removeChilds(void)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	while (m_child.size()) {
		std::shared_ptr<ValueBase> p = m_child[m_child.size()-1];
		
		p->setParent(nullptr);
		p.reset();
	
		m_child.pop_back();
	}
}

void ValueBase::onChildAdded(std::shared_ptr<ValueBase> child)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	for (auto& m_childAddedCallback : m_childAddedCallbacks) {
		(*m_childAddedCallback)(shared_from_this(), child);
	}
}

void ValueBase::addChildAddedCallback(const std::function<void(std::shared_ptr<ValueBase>, std::shared_ptr<ValueBase>)>* client)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	m_childAddedCallbacks.push_back(client);
}

void ValueBase::removeChildAddedCallback(const std::function<void(std::shared_ptr<ValueBase>, std::shared_ptr<ValueBase>)>* client)
{
	const std::lock_guard<std::recursive_mutex> lock(m_accessLock);

	m_childAddedCallbacks.remove(client);
}

std::map<VALUE_TYPE, std::string>  ValueBase::m_typeNameMap{
	std::make_pair(VALUE_TYPE::INVALID,			"INVALID"),
	std::make_pair(VALUE_TYPE::USER,			"USER"),
	std::make_pair(VALUE_TYPE::COMPLEX,			"COMPLEX"),
	std::make_pair(VALUE_TYPE::INT8,			"INT8"),
	std::make_pair(VALUE_TYPE::UINT8,			"UINT8"),
	std::make_pair(VALUE_TYPE::INT16,			"INT16"),
	std::make_pair(VALUE_TYPE::UINT16,			"UINT16"),
	std::make_pair(VALUE_TYPE::INT32,			"INT32"),
	std::make_pair(VALUE_TYPE::UINT32,			"UINT32"),
	std::make_pair(VALUE_TYPE::INT64,			"INT64"),
	std::make_pair(VALUE_TYPE::UINT64,			"UINT64"),
	std::make_pair(VALUE_TYPE::FLOAT,			"FLOAT"),
	std::make_pair(VALUE_TYPE::DOUBLE,			"DOUBLE"),
	std::make_pair(VALUE_TYPE::BOOL,			"BOOLEAN"),
	std::make_pair(VALUE_TYPE::STRING,			"STRING"),
	std::make_pair(VALUE_TYPE::SELECTION,		"SELECTION"),
	std::make_pair(VALUE_TYPE::SELECTED_ITEM,	"SELECTED_ITEM"),
	std::make_pair(VALUE_TYPE::ITEM_LIST,		"ITEM_LIST")
};

